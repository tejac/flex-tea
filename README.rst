Twitter Entity Analysis
===========================

**Install:**

.. code-block:: console

    composer install

**Run:**

1. In browser

Start the PHP built-in web server with

  .. code-block:: console

    COMPOSER_PROCESS_TIMEOUT=0 composer run

and browse to http://localhost:8888/.

Or simply open http://localhost/flex-tea/web if the project folder is in your web server document root.

2. CLI

To fetch, analyse and save data, run:

  .. code-block:: console

    php bin/console tea-analyse

To display the data, run:

   .. code-block:: console

    php bin/console tea-show
