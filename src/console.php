<?php

use Symfony\Component\Console\Application;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputOption;
use FlexTEA\Provider\GeneralServiceProvider;

$console = new Application('Flex TEA', '1.0');

$console->getDefinition()->addOption(new InputOption('--env', '-e', InputOption::VALUE_REQUIRED, 'The Environment name.', 'dev'));
$console->setDispatcher($app['dispatcher']);
$console
    ->register('tea-analyse')
    ->setDefinition(array(
        // new InputOption('some-option', null, InputOption::VALUE_NONE, 'Some help'),
    ))
    ->setDescription('Fetch latest tweets and analyse entities')
    ->setCode(function (InputInterface $input, OutputInterface $output) use ($app) {
        $output->writeln("Reading tweets...");
        $service = new GeneralServiceProvider($app);
        if (true === $service->readAndProcessTweets()) {
            $output->writeln("Analysis complete! Now you can run the tea-show command to see the results.");
        }
    })
;

$console
    ->register('tea-show')
    ->setDefinition(array(
        // new InputOption('some-option', null, InputOption::VALUE_NONE, 'Some help'),
    ))
    ->setDescription('Show saved tweets and entities')
    ->setCode(function (InputInterface $input, OutputInterface $output) use ($app) {
        // do something
        $fromDb = $app['db']->fetchAll('SELECT id, content, entity_type, entity FROM entities JOIN tweets ON tweets.id=entities.tweet_id');
        $table = new \Symfony\Component\Console\Helper\Table($output);
        $tableData = [];
        foreach ($fromDb as $row) {
            $tableData[] = [$row['id'], $row['content'], $row['entity_type'], $row['entity']];
        }
        $table
            ->setHeaders(array('ID', 'Content', 'Entity', 'Entity Type'))
            ->setRows($tableData);
        $table->render();
    })
;

return $console;
