<?php

namespace FlexTEA\Provider;

use Silex\Application;
use TwitterAPIExchange;
use Google\Cloud\Language\LanguageClient;

class GeneralServiceProvider
{
    protected $app;
    protected $settings = [
        'oauth_access_token' => "973555076722774019-uODvDBg4utZTDXWMwWyJV4CnVaGOVXb",
        'oauth_access_token_secret' => "wd6Zt0hXvgUI91w63Jjhf76o2MDgJu3PHeFj13a1VhCiC",
        'consumer_key' => "8Sakv3Bp6RUKndlGTntHxrhzU",
        'consumer_secret' => "vHexhXZs08xplPDwm1r6bW4atj96iyayPaDCmrqvkJx5fI9ITe"
    ];


    public function __construct(Application $app)
    {
        $this->app = $app;
    }

    public function readAndProcessTweets()
    {
        // URL and params
        $requestMethod = 'GET';
        $url = 'https://api.twitter.com/1.1/statuses/user_timeline.json';
        $getField = '?screen_name=Daniel_Sloss&count=50';

        // get data
        $twitter = new TwitterAPIExchange($this->settings);
        $data = $twitter
            ->setGetfield($getField)
            ->buildOauth($url, $requestMethod)
            ->performRequest();

        $data = json_decode($data, true);

        putenv("GOOGLE_APPLICATION_CREDENTIALS=".__DIR__."/../../../config/g-nat-lang.json");

        $projectId = 'focus-storm-197917';

        try {
            $language = new LanguageClient([
                'projectId' => $projectId
            ]);

            // save id and content to table tweets
            foreach ($data as $tweet) {
                $sql = "SELECT * FROM tweets WHERE id = ?";
                $post = $this->app['db']->fetchAssoc($sql, array((int) $tweet['id']));

                if (!$post) {
                    $this->app['db']->insert('tweets', [
                        'id' => $tweet['id'],
                        'content' => $tweet['text']
                    ]);
                } /*else {
                        $this->app['db']->update('tweets',
                            [ 'content' => $tweet['text']],
                            ['id' => $tweet['id']]
                        ]);

                }*/

                // check for hashtags and save to separate table
                if (!empty($tweet['entities']['hashtag'])) {
                    foreach ($tweet['entities']['hashtag'] as $htag) {
                        $this->app['db']->insert('hashtags', array(
                                'tweet_id' => $tweet['id'],
                                'hashtag' => $htag['text']
                            )
                        );
                    }
                }

                // analyse entities in text and save to separate table
                $annotation = $language->analyzeEntities($tweet['text']);
                foreach ($annotation->entities() as $entity) {
                    $this->app['db']->insert('entities', array(
                            'tweet_id' => $tweet['id'],
                            'entity_type' => $entity['type'],
                            'entity' => $entity['name'],
                            'url' => isset($entity['metadata']['wikipedia_url']) ? $entity['metadata']['wikipedia_url'] : '',
                            'json_result' => json_encode($entity)
                        )
                    );
                }
            }

            return true;

        } catch (\Exception $e) {
            return $e->getMessage();
        }
    }
}