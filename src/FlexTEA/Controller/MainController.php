<?php

namespace FlexTEA\Controller;

use FlexTEA\Provider\GeneralServiceProvider;
use Silex\Application;
use Symfony\Component\HttpFoundation\Request;

class MainController
{

    public function indexAction(Request $request, Application $app)
    {
        $go = $request->request->get('_analyse');

        if ($go) {
            $service = new GeneralServiceProvider($app);
            $service->readAndProcessTweets();
        }

        $fromDb = $app['db']->fetchAll('SELECT id, content, entity_type, entity, url FROM entities JOIN tweets ON tweets.id=entities.tweet_id');

        return $app['twig']->render('index.html.twig', ['data' => $fromDb]);
    }
}